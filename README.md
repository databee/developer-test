# Databee Full-stack Developer Test

The purpose of this test is to not only gauge the capabilities of potential applicants, but to also get an idea of their approach to development and problem-solving.

To complete this test, fork this repository to your own public Git repo, complete the task, and then email the URL of your repo to phil@databee.com.au. Include any instructions of any necessary steps to get your code working in the README file.

Whilst there is no time limit, this test is not intended to take a long time to complete so use your judgement.

## The Scenario

A core part of our new exams management platform is to give institutions (universities) the ability to check a remote exam venue's availability to host an exam.  To simulate this we will build a very simple system to display a host's availability over a three-day period.

## The Backend

Devise an SQL database schema to represent a venue's availability over time (more specifically for the next three months).  A venue is available to host exams at three times during the day - in the morning, in the afternoon and in the evening.

Document the necessary steps to randomly populate the database with periods of availability/unavailability.

## The Frontend

Create a simple web page that we can use to query the database.  It should comprise of a datepicker to select a date no more than three months in the future (and not in the past).  The page should then display a three day period - one day either side of the selected date - with the venue's availability for the three sessions outlined above.

This is not a design competition, but the frontend should be easy to understand and usable.

## Restrictions

Your backend solution must be written in PHP. You may support whatever version(s) of PHP that you wish, but you must include details of those requirements in your README file. The database engine to be used is also at your discretion.

You may use whatever tools and/or frameworks you wish to get the job done.

Your code must be fully commented to explain what is happening.

## Extra Credit

If you have time and are inspired to delve a little further into this expand your platform to include multiple host venues.  Your front-end interface should be updated to allow browsing the availability for multiple venues (either individually or combined).

## Any questions?

Email any questions to phil@databee.com.au.